package com.example.qurba.api.models;

import com.google.gson.annotations.SerializedName;

public class AuthenticatePayloadModel {


    @SerializedName("_id")
    private String _id;

    @SerializedName("role")
    private String role;

    @SerializedName("jwt")
    private String jwt;

    public String get_id() {
        return _id;
    }

    public String getRole() {
        return role;
    }

    public String getJwt() {
        return jwt;
    }


    public void set_id(String _id) {
        this._id = _id;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }
}

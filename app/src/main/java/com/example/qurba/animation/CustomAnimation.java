package com.example.qurba.animation;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

public class CustomAnimation {


    public static void fillScreen(final View view , final long delay) {

        Animation animation = new Animation() {

            private int mStartHeight = 0;
            private int mEndHeight;


            @Override
            public void initialize(int width, int height, int parentWidth, int parentHeight) {
                super.initialize(width, height, parentWidth, parentHeight);
                mStartHeight = 0;
                mEndHeight = view.getMeasuredHeight();
                setDuration(800);
                setStartOffset(delay);


            }

            @Override
            protected void applyTransformation(final float interpolatedTime, final Transformation t) {
                view.getLayoutParams().height = (int) (mStartHeight + (interpolatedTime * (mEndHeight - mStartHeight)));
                view.requestLayout();
            }


        };
        view.startAnimation(animation);

    }


    public static void scaleOutWithPulse(final View view) {

        Animation animation = new Animation() {

            private int startWidth = 0;
            private int startHeight = 0;
            private int endW;
            private int endH;

            @Override
            public void initialize(int width, int height, int parentWidth, int parentHeight) {
                super.initialize(width, height, parentWidth, parentHeight);
                setDuration(200);
                setStartOffset(400);
                endW = view.getMeasuredWidth();
                endH = view.getMeasuredHeight();
            }

            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                super.applyTransformation(interpolatedTime, t);
                view.getLayoutParams().width = (int) (startWidth + (interpolatedTime * (endW - startWidth)));
                view.getLayoutParams().height = (int) (startHeight + (interpolatedTime * (endH - startHeight)));
                view.requestLayout();


            }


        };
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {


                ObjectAnimator scaleDown = ObjectAnimator.ofPropertyValuesHolder(
                        view,
                        PropertyValuesHolder.ofFloat("scaleX", 1.3f),
                        PropertyValuesHolder.ofFloat("scaleY", 1.3f));
                scaleDown.setDuration(100);
                scaleDown.start();

                scaleDown.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {

                        ObjectAnimator scale = ObjectAnimator.ofPropertyValuesHolder(
                                view,
                                PropertyValuesHolder.ofFloat("scaleX", 0.9f),
                                PropertyValuesHolder.ofFloat("scaleY", 0.9f));
                        scale.setDuration(200);
                        scale.start();

                        scale.addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                ObjectAnimator scaleDown = ObjectAnimator.ofPropertyValuesHolder(
                                        view,
                                        PropertyValuesHolder.ofFloat("scaleX", 1f),
                                        PropertyValuesHolder.ofFloat("scaleY", 1f));
                                scaleDown.setDuration(300);
                                scaleDown.start();
                            }
                        });
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        view.startAnimation(animation);

    }

    public static void scaleOutWithPulse(final View view, final int delay, final int duration) {


        ObjectAnimator scaleDown = ObjectAnimator.ofPropertyValuesHolder(
                view,
                PropertyValuesHolder.ofFloat("scaleX", 0, 1.1f),
                PropertyValuesHolder.ofFloat("scaleY", 0, 1.1f));
        scaleDown.setDuration(duration);
        scaleDown.setStartDelay(delay);
        scaleDown.start();

        view.setVisibility(View.INVISIBLE);

        scaleDown.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animator) {

                ObjectAnimator scale = ObjectAnimator.ofPropertyValuesHolder(
                        view,
                        PropertyValuesHolder.ofFloat("scaleX", 0.9f),
                        PropertyValuesHolder.ofFloat("scaleY", 0.9f));
                scale.setDuration(200);
                scale.start();

                scale.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        ObjectAnimator scaleDown = ObjectAnimator.ofPropertyValuesHolder(
                                view,
                                PropertyValuesHolder.ofFloat("scaleX", 1f),
                                PropertyValuesHolder.ofFloat("scaleY", 1f));
                        scaleDown.setDuration(300);
                        scaleDown.start();
                    }
                });
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });


    }


    public static void pulseForEver(View view) {
        ObjectAnimator scaleDown = ObjectAnimator.ofPropertyValuesHolder(
                view,
                PropertyValuesHolder.ofFloat("scaleX", 1.2f),
                PropertyValuesHolder.ofFloat("scaleY", 1.2f));
        scaleDown.setDuration(310);
        scaleDown.setRepeatCount(ObjectAnimator.INFINITE);
        scaleDown.setRepeatMode(ObjectAnimator.REVERSE);
        scaleDown.start();

    }

}

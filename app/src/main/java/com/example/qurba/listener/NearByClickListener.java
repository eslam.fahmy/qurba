package com.example.qurba.listener;

import android.widget.ImageView;

import com.example.qurba.api.models.PlaceModel;

public interface NearByClickListener {
    void onPlaceClicked(PlaceModel placeModel ,  ImageView imageView , int position);
}

package com.example.qurba.api.models;

import com.google.gson.annotations.SerializedName;

public class AuthenticateModel {

    @SerializedName("type")
    private String type;

    @SerializedName("payload")
    private AuthenticatePayloadModel payload;


    public String getType() {
        return type;
    }

    public AuthenticatePayloadModel getPayload() {
        return payload;
    }
}

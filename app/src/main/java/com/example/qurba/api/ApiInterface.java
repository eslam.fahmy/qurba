package com.example.qurba.api;

import com.example.qurba.api.models.AuthenticateModel;
import com.example.qurba.api.models.NearByModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface ApiInterface {

    @Headers({
            "Content-Type: application/json",
            "version: 3.0",
            "language: en"
    })
    @POST("auth/login-guest")
    Call<AuthenticateModel> authenticate(@Body String body);


    @Headers({
            "Content-Type: application/json",
            "version: 3.0",
            "language: en"
    })
    @POST("places/places/nearby?page=1")
    Call<NearByModel> getNearBy(@Header("Authorization") String Authorization, @Body String body);

}

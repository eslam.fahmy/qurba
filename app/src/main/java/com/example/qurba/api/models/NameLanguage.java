package com.example.qurba.api.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class NameLanguage implements Parcelable {

    @SerializedName("en")
    private String en;

    @SerializedName("ar")
    private String ar;

    public String getEn() {
        return en;
    }

    public String getAr() {
        return ar;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.en);
        dest.writeString(this.ar);
    }

    public NameLanguage() {
    }

    protected NameLanguage(Parcel in) {
        this.en = in.readString();
        this.ar = in.readString();
    }

    public static final Parcelable.Creator<NameLanguage> CREATOR = new Parcelable.Creator<NameLanguage>() {
        @Override
        public NameLanguage createFromParcel(Parcel source) {
            return new NameLanguage(source);
        }

        @Override
        public NameLanguage[] newArray(int size) {
            return new NameLanguage[size];
        }
    };
}

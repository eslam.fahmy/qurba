package com.example.qurba.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LayoutAnimationController;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.qurba.R;
import com.example.qurba.adapter.NearByAdapter;
import com.example.qurba.animation.CustomAnimation;
import com.example.qurba.api.ApiClient;
import com.example.qurba.api.ApiInterface;
import com.example.qurba.api.models.AuthenticatePayloadModel;
import com.example.qurba.api.models.NearByModel;
import com.example.qurba.api.models.PlaceModel;
import com.example.qurba.databinding.ActivityMapsBinding;
import com.example.qurba.fragment.PlaceListViewFragment;
import com.example.qurba.listener.NearByClickListener;
import com.example.qurba.listener.OnLocationLoaded;
import com.example.qurba.services.LocationTrack;
import com.example.qurba.utils.Constants;
import com.example.qurba.utils.GlobalFunctions;
import com.example.qurba.utils.UtilsPreference;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, OnLocationLoaded, NearByClickListener, View.OnClickListener, GoogleMap.OnMarkerClickListener {


    private static final int LOCATION = 10;
    private GoogleMap mMap;
    private LocationTrack locationTrack;
    private ActivityMapsBinding binding;
    private LinearLayoutManager linearLayoutManager;
    private Animation fadeIn;
    private List<PlaceModel> placeModelList;
    private View mapView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_maps);

        binding.imgListView.setOnClickListener(this);
        binding.imgClear.setOnClickListener(this);
        binding.etSearch.addTextChangedListener(searchTextWatcher);
        binding.etSearch.setOnClickListener(this);
        KeyboardVisibilityEvent.setEventListener(this, keyboardVisibilityEventListener);

        linearLayoutManager = new LinearLayoutManager(MapsActivity.this, RecyclerView.HORIZONTAL, false);

        fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new DecelerateInterpolator()); //add this
        fadeIn.setDuration(400);

        if (GlobalFunctions.servicesOK(this)) {
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            assert mapFragment != null;
            mapFragment.getMapAsync(this);
            mapView = mapFragment.getView();
        } else {
            if (binding.permissionView.layoutPermission.getVisibility() == View.GONE) {
                binding.permissionView.layoutPermission.setVisibility(View.VISIBLE);
                CustomAnimation.fillScreen(binding.permissionView.layoutPermission, 400);
                binding.permissionView.tvLocationHint.setVisibility(View.GONE);
                binding.permissionView.tvGrantPermission.setText(getResources().getString(R.string.google_place_services));

            }
        }


    }

    private void loadingDate() {
        List<PlaceModel> placeModelList = new ArrayList<>();
        PlaceModel model = new PlaceModel();
        placeModelList.add(model);
        placeModelList.add(model);
        placeModelList.add(model);
        NearByAdapter nearByAdapter = new NearByAdapter(MapsActivity.this, placeModelList, null, MapsActivity.this, true);
        binding.rvNearBy.setLayoutManager(linearLayoutManager);
        binding.rvNearBy.setAdapter(nearByAdapter);
    }

    private TextWatcher searchTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (charSequence.toString().isEmpty())
                binding.imgClear.setVisibility(View.INVISIBLE);
            else
                binding.imgClear.setVisibility(View.VISIBLE);
        }

        @Override
        public void afterTextChanged(Editable editable) {
        }
    };


    private KeyboardVisibilityEventListener keyboardVisibilityEventListener = new KeyboardVisibilityEventListener() {
        @Override
        public void onVisibilityChanged(boolean isOpen) {
            if (!isOpen) {
                if (GlobalFunctions.isNetworkAvailable(MapsActivity.this) && ( placeModelList != null && placeModelList.size() > 0 ) ) {
                    binding.rvNearBy.startAnimation(fadeIn);
                    binding.rvNearBy.setVisibility(View.VISIBLE);
                }
            } else {
                binding.rvNearBy.setVisibility(View.GONE);
            }
        }
    };

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        getLocation();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        try {
            if (requestCode == LOCATION) {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    getLocation();
                } else {
                    if (binding.permissionView.layoutPermission.getVisibility() == View.GONE) {
                        binding.permissionView.layoutPermission.setVisibility(View.VISIBLE);
                        CustomAnimation.fillScreen(binding.permissionView.layoutPermission, 0);
                        CustomAnimation.pulseForEver(binding.permissionView.tvGrantPermission);
                        binding.permissionView.tvGrantPermission.setOnClickListener(MapsActivity.this);
                    }

                }
            }
        } catch (
                Exception ignored) {
            getLocation();
        }

    }

    @SuppressLint("NewApi")
    private void getLocation() {

        int permissionCheckFineLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int permissionCheckCrosLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);

        if (permissionCheckFineLocation == PackageManager.PERMISSION_DENIED && permissionCheckCrosLocation == PackageManager.PERMISSION_DENIED) {
            requestPermissions(new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
            }, LOCATION);

        } else {

            binding.permissionView.layoutPermission.setVisibility(View.GONE);
            loadingDate();

            if (locationTrack == null)
                locationTrack = new LocationTrack(this);

            if (!locationTrack.canGetLocation()) {
                locationTrack.showSettingsAlert();
            } else {
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);

                if (mapView != null &&
                        mapView.findViewById(Integer.parseInt("1")) != null) {
                    View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
                    layoutParams.addRule(RelativeLayout.ABOVE, R.id.rvNearBy);
                    layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
                    layoutParams.setMargins(30, 30, 30, 30);
                }

                mMap.setOnMarkerClickListener(this);

                LatLng currentLocation = new LatLng(locationTrack.getLatitude(), locationTrack.getLongitude());
                CameraUpdate location = CameraUpdateFactory.newLatLngZoom(currentLocation, Float.parseFloat("14.1"));
                mMap.animateCamera(location);
                if (GlobalFunctions.isNetworkAvailable(MapsActivity.this)) {
                    binding.tvNoInternet.setVisibility(View.GONE);
                    loadNearPlaces(locationTrack.getLatitude(), locationTrack.getLongitude());
                } else {
                    binding.tvNoInternet.setVisibility(View.VISIBLE);
                    binding.rvNearBy.setVisibility(View.GONE);
                    CustomAnimation.pulseForEver(binding.tvNoInternet);
                }
            }

        }


    }

    private void loadNearPlaces(double latitude, double longitude) {

        AuthenticatePayloadModel user = UtilsPreference.getInstance(this).getUser();
        if (user == null || user.getJwt() == null || user.getJwt().isEmpty())
            return;
        String token = "JWT " + user.getJwt();
        String body = "{\"payload\": { \"lng\": " + longitude + ",    \"lat\": " + latitude + "   }}";
        ApiInterface mApiService = ApiClient.getInterfaceService();
        Call<NearByModel> call = mApiService.getNearBy(token, body);
        call.enqueue(new Callback<NearByModel>() {
            @Override
            public void onResponse(@NonNull Call<NearByModel> call, @NonNull Response<NearByModel> response) {
                if (response.code() != 200 || response.body() == null) {
                    Toast.makeText(MapsActivity.this, MapsActivity.this.getResources().getString(R.string.error), Toast.LENGTH_SHORT).show();
                    return;
                }
                NearByModel nearByModel = response.body();
                if (nearByModel.getType() == null || nearByModel.getType().isEmpty() || !nearByModel.getType().equals(Constants.SUCCESS)) {
                    Toast.makeText(MapsActivity.this, MapsActivity.this.getResources().getString(R.string.error), Toast.LENGTH_SHORT).show();
                    return;
                }
                placeModelList = nearByModel.getPayload();

                if (placeModelList == null || placeModelList.size() == 0) {
                    binding.tvNoInternet.setText(getResources().getString(R.string.no_place_near_to_you));
                    binding.tvNoInternet.setVisibility(View.VISIBLE);
                    binding.rvNearBy.setVisibility(View.GONE);
                    return;
                }

                final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(MapsActivity.this, R.anim.layout_animation_fall_up);
                NearByAdapter nearByAdapter = new NearByAdapter(MapsActivity.this, placeModelList, MapsActivity.this, MapsActivity.this, false);
                binding.rvNearBy.setLayoutManager(linearLayoutManager);
                binding.rvNearBy.setAdapter(nearByAdapter);
                binding.rvNearBy.setLayoutAnimation(controller);
                CustomAnimation.scaleOutWithPulse(binding.layoutHeader);


            }

            @Override
            public void onFailure(@NonNull Call<NearByModel> call, @NonNull Throwable t) {
                binding.tvNoInternet.setVisibility(View.VISIBLE);
                binding.rvNearBy.setVisibility(View.GONE);
                CustomAnimation.pulseForEver(binding.tvNoInternet);

            }
        });
    }


    @Override
    public void populateOnMap(PlaceModel place, int position) {
        if (place != null && place.getLocation() != null && place.getLocation().getCoordinates().length >= 2) {
            LatLng placeLocation = new LatLng(place.getLocation().getCoordinates()[0], place.getLocation().getCoordinates()[1]);
            MarkerOptions marker = new MarkerOptions();
            marker.position(placeLocation);
            if (place.getName() != null && place.getName().getEn() != null)
                marker.title(place.getName().getEn());
            Marker placeMarker = mMap.addMarker(marker);
            placeMarker.setTag(position);
            CameraUpdate location = CameraUpdateFactory.newLatLngZoom(placeLocation, Float.parseFloat("14.1"));
            mMap.animateCamera(location);


        }
    }

    @Override
    public void onPlaceClicked(PlaceModel placeModel, ImageView imageView, int position) {

        Intent intent = new Intent(this, PlaceDetailsActivity.class);
        intent.putExtra(Constants.TRANSITION, Constants.TRANSITION_IMAGE + position);
        intent.putExtra(Constants.PAYLOAD, placeModel);
        @SuppressLint("CutPasteId")
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, imageView, Constants.TRANSITION_IMAGE + position);
        this.startActivity(intent, options.toBundle());

    }


    @Override
    public void onClick(View view) {
        if (view == null)
            return;
        switch (view.getId()) {
            case R.id.imgListView:

                InputMethodManager keyboard = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
                assert keyboard != null;
                keyboard.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);

                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.mainContent, PlaceListViewFragment.newInstance(placeModelList))
                        .addToBackStack(PlaceListViewFragment.TAG)
                        .commit();
                break;
            case R.id.imgClear:
                binding.etSearch.setText(null);
                break;
            case R.id.tvGrantPermission:
                getLocation();
                break;
            case R.id.etSearch:
                binding.rvNearBy.setVisibility(View.GONE);
                break;
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mainContent);
            if (fragment instanceof PlaceListViewFragment)
                return true;
            else
                finish();


        }
        return false;

    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        if (binding.rvNearBy != null && marker.getTag() != null)
            binding.rvNearBy.smoothScrollToPosition((Integer) marker.getTag());
        return false;
    }
}

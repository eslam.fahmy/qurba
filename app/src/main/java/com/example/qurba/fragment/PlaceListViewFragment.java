package com.example.qurba.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.qurba.R;
import com.example.qurba.activity.PlaceDetailsActivity;
import com.example.qurba.adapter.NearByAdapter;
import com.example.qurba.animation.CustomAnimation;
import com.example.qurba.api.ApiClient;
import com.example.qurba.api.ApiInterface;
import com.example.qurba.api.models.AuthenticatePayloadModel;
import com.example.qurba.api.models.NearByModel;
import com.example.qurba.api.models.PlaceModel;
import com.example.qurba.databinding.FragmentPlaceDetailsBinding;
import com.example.qurba.listener.NearByClickListener;
import com.example.qurba.services.LocationTrack;
import com.example.qurba.utils.Constants;
import com.example.qurba.utils.GlobalFunctions;
import com.example.qurba.utils.UtilsPreference;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlaceListViewFragment extends Fragment implements NearByClickListener, View.OnClickListener {

    public static final String TAG = "PlaceListViewFragment";
    private static final int LOCATION = 10;
    private FragmentPlaceDetailsBinding binding;
    private List<PlaceModel> placeModelList;
    private LayoutAnimationController controller;
    private LinearLayoutManager linearLayoutManager;
    private LocationTrack locationTrack;


    private PlaceListViewFragment(List<PlaceModel> placeModelList) {
        this.placeModelList = placeModelList;
    }


    public static Fragment newInstance(List<PlaceModel> placeModelList) {
        return new PlaceListViewFragment(placeModelList);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_place_details, container, false);
        binding.imgClear.setOnClickListener(this);
        binding.imgMapView.setOnClickListener(this);
        binding.etSearch.addTextChangedListener(searchTextWatcher);
        controller = AnimationUtils.loadLayoutAnimation(PlaceListViewFragment.this.getActivity(), R.anim.layout_animation_fall_down);
        linearLayoutManager = new LinearLayoutManager(PlaceListViewFragment.this.getActivity(), RecyclerView.VERTICAL, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (placeModelList == null)
            getLocation();
        else if (placeModelList.size() == 0) {
            binding.tvNoInternet.setText(getResources().getString(R.string.no_place_near_to_you));
            binding.tvNoInternet.setVisibility(View.VISIBLE);
            binding.rvNearBy.setVisibility(View.GONE);
        } else {
            NearByAdapter nearByAdapter = new NearByAdapter(PlaceListViewFragment.this.getActivity(), placeModelList, PlaceListViewFragment.this, null, false);
            binding.rvNearBy.setLayoutManager(linearLayoutManager);
            binding.rvNearBy.setAdapter(nearByAdapter);
            binding.rvNearBy.setLayoutAnimation(controller);
        }
    }

    private void loadingDate() {
        List<PlaceModel> placeModelList = new ArrayList<>();
        PlaceModel model = new PlaceModel();
        placeModelList.add(model);
        placeModelList.add(model);
        placeModelList.add(model);
        NearByAdapter nearByAdapter = new NearByAdapter(PlaceListViewFragment.this.getActivity(), placeModelList, null, null, true);
        binding.rvNearBy.setLayoutManager(linearLayoutManager);
        binding.rvNearBy.setAdapter(nearByAdapter);
        binding.rvNearBy.setLayoutAnimation(controller);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        try {
            if (requestCode == LOCATION) {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    getLocation();
                } else {
                    if (binding.permissionView.layoutPermission.getVisibility() == View.GONE) {
                        binding.permissionView.layoutPermission.setVisibility(View.VISIBLE);
                        CustomAnimation.fillScreen(binding.permissionView.layoutPermission, 0);
                        CustomAnimation.pulseForEver(binding.permissionView.tvGrantPermission);
                        binding.permissionView.tvGrantPermission.setOnClickListener(PlaceListViewFragment.this);
                    }

                }
            }
        } catch (Exception ignored) {
            getLocation();
        }
    }

    private void getLocation() {

        if (PlaceListViewFragment.this.getContext() == null)
            return;

        int permissionCheckFineLocation = ContextCompat.checkSelfPermission(PlaceListViewFragment.this.getContext(), Manifest.permission.ACCESS_FINE_LOCATION);
        int permissionCheckCrosLocation = ContextCompat.checkSelfPermission(PlaceListViewFragment.this.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION);

        if (permissionCheckFineLocation == PackageManager.PERMISSION_DENIED && permissionCheckCrosLocation == PackageManager.PERMISSION_DENIED) {
            requestPermissions(new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
            }, LOCATION);

        } else {

            binding.permissionView.layoutPermission.setVisibility(View.GONE);
            loadingDate();

            if (locationTrack == null)
                locationTrack = new LocationTrack(PlaceListViewFragment.this.getContext());

            if (!locationTrack.canGetLocation()) {
                locationTrack.showSettingsAlert();
            } else {
                if (GlobalFunctions.isNetworkAvailable(PlaceListViewFragment.this.getContext())) {
                    binding.tvNoInternet.setVisibility(View.GONE);
                    loadNearPlaces(locationTrack.getLatitude(), locationTrack.getLongitude());
                } else {
                    binding.tvNoInternet.setVisibility(View.VISIBLE);
                    binding.rvNearBy.setVisibility(View.GONE);
                    CustomAnimation.pulseForEver(binding.tvNoInternet);
                }
            }

        }


    }


    private TextWatcher searchTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (charSequence.toString().isEmpty())
                binding.imgClear.setVisibility(View.INVISIBLE);
            else
                binding.imgClear.setVisibility(View.VISIBLE);
        }

        @Override
        public void afterTextChanged(Editable editable) {
        }
    };

    private void loadNearPlaces(double latitude, double longitude) {

        AuthenticatePayloadModel user = UtilsPreference.getInstance(PlaceListViewFragment.this.getActivity()).getUser();
        if (user == null || user.getJwt() == null || user.getJwt().isEmpty())
            return;
        String token = "JWT " + user.getJwt();
        String body = "{\"payload\": { \"lng\": " + longitude + ",    \"lat\": " + latitude + "   }}";
        ApiInterface mApiService = ApiClient.getInterfaceService();
        Call<NearByModel> call = mApiService.getNearBy(token, body);
        call.enqueue(new Callback<NearByModel>() {
            @Override
            public void onResponse(@NonNull Call<NearByModel> call, @NonNull Response<NearByModel> response) {

                if (PlaceListViewFragment.this.getActivity() == null)
                    return;

                if (!isAdded())
                    return;

                if (response.code() != 200 || response.body() == null) {
                    Toast.makeText(PlaceListViewFragment.this.getActivity(), getResources().getString(R.string.error), Toast.LENGTH_SHORT).show();
                    return;
                }
                NearByModel nearByModel = response.body();
                if (nearByModel.getType() == null || nearByModel.getType().isEmpty() || !nearByModel.getType().equals(Constants.SUCCESS)) {
                    Toast.makeText(PlaceListViewFragment.this.getActivity(), getResources().getString(R.string.error), Toast.LENGTH_SHORT).show();
                    return;
                }
                List<PlaceModel> placeModelList = nearByModel.getPayload();
                if (placeModelList == null || placeModelList.size() == 0) {
                    binding.tvNoInternet.setText(getResources().getString(R.string.no_place_near_to_you));
                    binding.tvNoInternet.setVisibility(View.VISIBLE);
                    binding.rvNearBy.setVisibility(View.GONE);
                    return;
                }

                NearByAdapter nearByAdapter = new NearByAdapter(PlaceListViewFragment.this.getActivity(), placeModelList, PlaceListViewFragment.this, null, false);
                binding.rvNearBy.setLayoutManager(linearLayoutManager);
                binding.rvNearBy.setAdapter(nearByAdapter);
                binding.rvNearBy.setLayoutAnimation(controller);


            }

            @Override
            public void onFailure(@NonNull Call<NearByModel> call, @NonNull Throwable t) {
                binding.tvNoInternet.setVisibility(View.VISIBLE);
                binding.rvNearBy.setVisibility(View.GONE);
                CustomAnimation.pulseForEver(binding.tvNoInternet);

            }
        });
    }

    @Override
    public void onPlaceClicked(PlaceModel placeModel, ImageView imageView, int position) {

        if (PlaceListViewFragment.this.getActivity() == null)
            return;

        Intent intent = new Intent(PlaceListViewFragment.this.getContext(), PlaceDetailsActivity.class);
        intent.putExtra(Constants.TRANSITION, Constants.TRANSITION_IMAGE + position);
        intent.putExtra(Constants.PAYLOAD, placeModel);
        @SuppressLint("CutPasteId")
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(PlaceListViewFragment.this.getActivity(), imageView, Constants.TRANSITION_IMAGE + position);
        this.startActivity(intent, options.toBundle());


    }

    @Override
    public void onClick(View view) {
        if (view == null)
            return;
        switch (view.getId()) {
            case R.id.imgMapView:
                if (PlaceListViewFragment.this.getActivity() == null)
                    return;

                InputMethodManager keyboard = (InputMethodManager) PlaceListViewFragment.this.getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                assert keyboard != null;
                keyboard.hideSoftInputFromWindow(PlaceListViewFragment.this.getActivity().getWindow().getDecorView().getRootView().getWindowToken(), 0);

                PlaceListViewFragment.this.getActivity().getSupportFragmentManager().popBackStack();
                break;
            case R.id.imgClear:
                binding.etSearch.setText(null);
                break;
            case R.id.tvGrantPermission:
                getLocation();
                break;
        }
    }


}

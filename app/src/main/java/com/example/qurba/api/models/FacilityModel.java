package com.example.qurba.api.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.gson.annotations.SerializedName;

import androidx.databinding.BindingAdapter;

public class FacilityModel implements Parcelable {

    @SerializedName("name")
    private NameLanguage name;

    @SerializedName("iconUrl")
    private String iconUrl;

    @SerializedName("mobileIconUrl")
    private String mobileIconUrl;

    @SerializedName("id")
    private String id;

    public NameLanguage getName() {
        return name;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public String getMobileIconUrl() {
        return mobileIconUrl;
    }

    public String getId() {
        return id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @BindingAdapter("imageURL")
    public static void loadImage(ImageView view, String imageUrl) {
        if (view != null && imageUrl != null && !imageUrl.isEmpty())
            Glide.with(view.getContext()).asBitmap().load(imageUrl).into(view);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.name, flags);
        dest.writeString(this.iconUrl);
        dest.writeString(this.mobileIconUrl);
        dest.writeString(this.id);
    }

    public FacilityModel() {
    }

    protected FacilityModel(Parcel in) {
        this.name = in.readParcelable(NameLanguage.class.getClassLoader());
        this.iconUrl = in.readString();
        this.mobileIconUrl = in.readString();
        this.id = in.readString();
    }

    public static final Parcelable.Creator<FacilityModel> CREATOR = new Parcelable.Creator<FacilityModel>() {
        @Override
        public FacilityModel createFromParcel(Parcel source) {
            return new FacilityModel(source);
        }

        @Override
        public FacilityModel[] newArray(int size) {
            return new FacilityModel[size];
        }
    };
}

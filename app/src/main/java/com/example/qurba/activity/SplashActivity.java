package com.example.qurba.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.example.qurba.R;
import com.example.qurba.api.ApiClient;
import com.example.qurba.api.ApiInterface;
import com.example.qurba.api.models.AuthenticateModel;
import com.example.qurba.api.models.AuthenticatePayloadModel;
import com.example.qurba.utils.Constants;
import com.example.qurba.utils.GlobalFunctions;
import com.example.qurba.utils.UtilsPreference;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final AuthenticatePayloadModel user = UtilsPreference.getInstance(this).getUser();
        if (user.getJwt() == null || user.getJwt().isEmpty())
            authenticate();
        else {
            startActivity(new Intent(this, MapsActivity.class));
            finish();
        }
    }

    private void authenticate() {
        ApiInterface mApiService = ApiClient.getInterfaceService();
        String body = "{\n" +
                "    \"payload\": {\n" +
                "        \"deviceId\": \"" + GlobalFunctions.getDeviceID(SplashActivity.this) + "\"\n" +
                "    }\n" +
                "}";
        Call<AuthenticateModel> call = mApiService.authenticate(body);
        call.enqueue(new Callback<AuthenticateModel>() {
            @Override
            public void onResponse(@NonNull Call<AuthenticateModel> call, @NonNull Response<AuthenticateModel> response) {
                if (response.code() != 200 || response.body() == null) {
                    Toast.makeText(SplashActivity.this, SplashActivity.this.getResources().getString(R.string.error), Toast.LENGTH_SHORT).show();
                    return;
                }
                AuthenticateModel authenticateModel = response.body();
                if (!authenticateModel.getType().equals(Constants.SUCCESS) || authenticateModel.getPayload() == null || authenticateModel.getPayload().getJwt() == null || authenticateModel.getPayload().getJwt().isEmpty()) {
                    Toast.makeText(SplashActivity.this, SplashActivity.this.getResources().getString(R.string.error), Toast.LENGTH_SHORT).show();
                    return;
                }
                AuthenticatePayloadModel user = new AuthenticatePayloadModel();
                user.setRole(authenticateModel.getPayload().getRole());
                user.setJwt(authenticateModel.getPayload().getJwt());
                user.set_id(authenticateModel.getPayload().get_id());
                UtilsPreference.getInstance(SplashActivity.this).saveUser(user);
                startActivity(new Intent(SplashActivity.this, MapsActivity.class));
                finish();
            }

            @Override
            public void onFailure(@NonNull Call<AuthenticateModel> call, @NonNull Throwable t) {
                new AlertDialog.Builder(SplashActivity.this)
                        .setMessage(getResources().getString(R.string.check_you_internet_connection))
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                SplashActivity.this.finish();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

            }
        });
    }
}

package com.example.qurba.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.qurba.R;
import com.example.qurba.api.models.CategoryModel;
import com.example.qurba.api.models.PlaceModel;
import com.example.qurba.databinding.RvNearByItemBinding;
import com.example.qurba.listener.NearByClickListener;
import com.example.qurba.listener.OnLocationLoaded;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class NearByAdapter extends RecyclerView.Adapter<NearByAdapter.ViewHolder> {

    private Activity context;
    private List<PlaceModel> productModelList;
    private NearByClickListener listener;
    private OnLocationLoaded loaded;
    private boolean isLoading;

    public NearByAdapter(Activity context, List<PlaceModel> productModelList, NearByClickListener listener, OnLocationLoaded loaded, boolean isLoading) {
        this.context = context;
        this.productModelList = productModelList;
        this.listener = listener;
        this.isLoading = isLoading;
        this.loaded = loaded;
    }

    @NonNull
    @Override
    public NearByAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RvNearByItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.rv_near_by_item, parent, false);
        binding.setListener(listener);
        return new NearByAdapter.ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final NearByAdapter.ViewHolder holder, final int position) {
        if (position != RecyclerView.NO_POSITION)
            holder.setViewModel(productModelList.get(position), position);
    }

    @Override
    public int getItemCount() {
        return productModelList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private RvNearByItemBinding binding;

        ViewHolder(final RvNearByItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }

        void bind() {
            if (binding == null) {
                binding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (binding != null) {
                binding.unbind();
            }
        }

        void setViewModel(PlaceModel placeModel, int position) {

            if (binding != null) {
                if (isLoading) {
                    binding.imgLoader.startShimmerAnimation();
                    binding.nameLoader.startShimmerAnimation();
                    binding.categoriesLoader.startShimmerAnimation();
                    binding.rateLoader.startShimmerAnimation();
                    binding.addressLoader.startShimmerAnimation();
                } else {
                    binding.setPlace(placeModel);
                    binding.setPosition(position);
                    binding.tvName.setBackground(null);
                    binding.tvCategories.setBackground(null);
                    binding.rate.setBackground(null);
                    binding.tvAddress.setBackground(null);

                    assert placeModel != null;

                    try {


                        StringBuilder categories = new StringBuilder();
                        for (CategoryModel category : placeModel.getCategories()) {
                            categories.append(category.getName().getEn());
                            categories.append(" , ");
                        }
                        categories.deleteCharAt(categories.lastIndexOf(","));
                        binding.tvCategories.setText(categories.toString());

                    } catch (Exception ignored) {
                    }


                    if (loaded != null)
                        loaded.populateOnMap(placeModel, getAdapterPosition());


                }


            }
        }

    }


    @Override
    public void onViewAttachedToWindow(@NonNull NearByAdapter.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();

    }

    @Override
    public void onViewDetachedFromWindow(@NonNull NearByAdapter.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }


}



package com.example.qurba.api.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class CountryNameModel implements Parcelable {

    @SerializedName("en")
    private String en;

    @SerializedName("ar")
    private String ar;

    @SerializedName("abbr")
    private String abbr;

    public String getEn() {
        return en;
    }

    public String getAr() {
        return ar;
    }

    public String getAbbr() {
        return abbr;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.en);
        dest.writeString(this.ar);
        dest.writeString(this.abbr);
    }

    public CountryNameModel() {
    }

    protected CountryNameModel(Parcel in) {
        this.en = in.readString();
        this.ar = in.readString();
        this.abbr = in.readString();
    }

    public static final Parcelable.Creator<CountryNameModel> CREATOR = new Parcelable.Creator<CountryNameModel>() {
        @Override
        public CountryNameModel createFromParcel(Parcel source) {
            return new CountryNameModel(source);
        }

        @Override
        public CountryNameModel[] newArray(int size) {
            return new CountryNameModel[size];
        }
    };
}

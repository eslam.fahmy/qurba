package com.example.qurba.listener;

import com.example.qurba.api.models.PlaceModel;

public interface OnLocationLoaded {

    void populateOnMap (PlaceModel place , int position);
}

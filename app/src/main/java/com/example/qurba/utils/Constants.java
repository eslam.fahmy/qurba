package com.example.qurba.utils;

public class Constants {
    public static final String USER_ID = "USER_ID";
    public static final String USER_TOKEN = "USER_TOKEN";
    public static final String USER_ROLE = "USER_ROLE";
    public static final String SUCCESS = "SUCCESS";
    public static final String TRANSITION = "TRANSITION";
    public static final String PAYLOAD = "PAYLOAD";
    public static final String TRANSITION_IMAGE = "TRANSITION_IMAGE";
}

package com.example.qurba.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.qurba.R;
import com.example.qurba.animation.CustomAnimation;
import com.example.qurba.api.models.CategoryModel;
import com.example.qurba.databinding.RvCategoriesItemBinding;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.ViewHolder> {

    private Activity context;
    private List<CategoryModel> list;
    private int animationScaleFactor = 800;

    public CategoriesAdapter(Activity context, List<CategoryModel> list) {
        this.context = context;
        this.list = list;

    }

    @NonNull
    @Override
    public CategoriesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RvCategoriesItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.rv_categories_item, parent, false);
        return new CategoriesAdapter.ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final CategoriesAdapter.ViewHolder holder, final int position) {
        if (position != RecyclerView.NO_POSITION)
            holder.setViewModel(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private RvCategoriesItemBinding binding;

        ViewHolder(final RvCategoriesItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }

        void bind() {
            if (binding == null) {
                binding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (binding != null) {
                binding.unbind();
            }
        }

        void setViewModel(CategoryModel model) {
            if (binding != null) {
                binding.setCategory(model);
             //   CustomAnimation.scaleOutWithPulse(binding.layoutContainer, animationScaleFactor, 300);
             //   animationScaleFactor = animationScaleFactor + 100;
            }
        }

    }


    @Override
    public void onViewAttachedToWindow(@NonNull CategoriesAdapter.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull CategoriesAdapter.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }


}




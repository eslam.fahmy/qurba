package com.example.qurba.api.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class CategoryModel implements Parcelable {

    @SerializedName("name")
    private NameLanguage name;

    @SerializedName("iconUrl")
    private String iconUrl;

    @SerializedName("hoveredIconUrl")
    private String hoveredIconUrl;

    @SerializedName("id")
    private String id;

    public NameLanguage getName() {
        return name;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public String getHoveredIconUrl() {
        return hoveredIconUrl;
    }

    public String getId() {
        return id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.name, flags);
        dest.writeString(this.iconUrl);
        dest.writeString(this.hoveredIconUrl);
        dest.writeString(this.id);
    }

    public CategoryModel() {
    }

    protected CategoryModel(Parcel in) {
        this.name = in.readParcelable(NameLanguage.class.getClassLoader());
        this.iconUrl = in.readString();
        this.hoveredIconUrl = in.readString();
        this.id = in.readString();
    }

    public static final Parcelable.Creator<CategoryModel> CREATOR = new Parcelable.Creator<CategoryModel>() {
        @Override
        public CategoryModel createFromParcel(Parcel source) {
            return new CategoryModel(source);
        }

        @Override
        public CategoryModel[] newArray(int size) {
            return new CategoryModel[size];
        }
    };
}

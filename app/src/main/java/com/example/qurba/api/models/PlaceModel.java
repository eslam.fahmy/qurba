package com.example.qurba.api.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.qurba.R;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import androidx.databinding.BindingAdapter;


public class PlaceModel implements Parcelable {

    @SerializedName("name")
    private NameLanguage name;

    @SerializedName("branchName")
    private NameLanguage branchName;

    @SerializedName("address")
    private AddressModel address;

    @SerializedName("uniqueName")
    private String uniqueName;

    @SerializedName("placeRating")
    private float placeRating;

    @SerializedName("numberOfReviews")
    private float numberOfReviews;

    @SerializedName("offerIds")
    private List<String> offerIds;

    @SerializedName("_id")
    private String _id;

    @SerializedName("placeProfilePictureUrl")
    private String placeProfilePictureUrl;

    @SerializedName("facilities")
    private List<FacilityModel> facilities;

    @SerializedName("categories")
    private List<CategoryModel> categories;

    @SerializedName("subcategories")
    private List<CategoryModel> subcategories;

    @SerializedName("location")
    private LocationModel location;

    @SerializedName("offersCount")
    private int offersCount;

    @SerializedName("followersCount")
    private int followersCount;

    @SerializedName("activeOffersCount")
    private int activeOffersCount;


    @SerializedName("hasOpeningTimes")
    private boolean hasOpeningTimes;

    @SerializedName("openNow")
    private boolean openNow;

    @SerializedName("id")
    private String id;

    @SerializedName("isFollowedByUser")
    private boolean isFollowedByUser;

    public NameLanguage getName() {
        return name;
    }

    public NameLanguage getBranchName() {
        return branchName;
    }

    public AddressModel getAddress() {
        return address;
    }

    public String getUniqueName() {
        return uniqueName;
    }

    public float getPlaceRating() {
        return placeRating;
    }

    public float getNumberOfReviews() {
        return numberOfReviews;
    }

    public List<String> getOfferIds() {
        return offerIds;
    }

    public String get_id() {
        return _id;
    }

    public String getPlaceProfilePictureUrl() {
        return placeProfilePictureUrl;
    }

    public List<FacilityModel> getFacilities() {
        return facilities;
    }

    public List<CategoryModel> getCategories() {
        return categories;
    }

    public List<CategoryModel> getSubcategories() {
        return subcategories;
    }


    public int getOffersCount() {
        return offersCount;
    }

    public int getFollowersCount() {
        return followersCount;
    }

    public int getActiveOffersCount() {
        return activeOffersCount;
    }

    public boolean isHasOpeningTimes() {
        return hasOpeningTimes;
    }

    public boolean isOpenNow() {
        return openNow;
    }

    public String getId() {
        return id;
    }

    public boolean isFollowedByUser() {
        return isFollowedByUser;
    }

    public LocationModel getLocation() {
        return location;
    }


    @BindingAdapter("imageURL")
    public static void loadImage(ImageView view, String imageUrl) {
        if (view != null && imageUrl != null && !imageUrl.isEmpty())
            Glide.with(view.getContext()).asBitmap().load(imageUrl).placeholder(R.color.gray).diskCacheStrategy(DiskCacheStrategy.DATA).into(view);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.name, flags);
        dest.writeParcelable(this.branchName, flags);
        dest.writeParcelable(this.address, flags);
        dest.writeString(this.uniqueName);
        dest.writeFloat(this.placeRating);
        dest.writeFloat(this.numberOfReviews);
        dest.writeStringList(this.offerIds);
        dest.writeString(this._id);
        dest.writeString(this.placeProfilePictureUrl);
        dest.writeList(this.facilities);
        dest.writeList(this.categories);
        dest.writeList(this.subcategories);
        dest.writeParcelable(this.location, flags);
        dest.writeInt(this.offersCount);
        dest.writeInt(this.followersCount);
        dest.writeInt(this.activeOffersCount);
        dest.writeByte(this.hasOpeningTimes ? (byte) 1 : (byte) 0);
        dest.writeByte(this.openNow ? (byte) 1 : (byte) 0);
        dest.writeString(this.id);
        dest.writeByte(this.isFollowedByUser ? (byte) 1 : (byte) 0);
    }

    public PlaceModel() {
    }

    protected PlaceModel(Parcel in) {
        this.name = in.readParcelable(NameLanguage.class.getClassLoader());
        this.branchName = in.readParcelable(NameLanguage.class.getClassLoader());
        this.address = in.readParcelable(AddressModel.class.getClassLoader());
        this.uniqueName = in.readString();
        this.placeRating = in.readFloat();
        this.numberOfReviews = in.readFloat();
        this.offerIds = in.createStringArrayList();
        this._id = in.readString();
        this.placeProfilePictureUrl = in.readString();
        this.facilities = new ArrayList<FacilityModel>();
        in.readList(this.facilities, FacilityModel.class.getClassLoader());
        this.categories = new ArrayList<CategoryModel>();
        in.readList(this.categories, CategoryModel.class.getClassLoader());
        this.subcategories = new ArrayList<CategoryModel>();
        in.readList(this.subcategories, CategoryModel.class.getClassLoader());
        this.location = in.readParcelable(LocationModel.class.getClassLoader());
        this.offersCount = in.readInt();
        this.followersCount = in.readInt();
        this.activeOffersCount = in.readInt();
        this.hasOpeningTimes = in.readByte() != 0;
        this.openNow = in.readByte() != 0;
        this.id = in.readString();
        this.isFollowedByUser = in.readByte() != 0;
    }

    public static final Parcelable.Creator<PlaceModel> CREATOR = new Parcelable.Creator<PlaceModel>() {
        @Override
        public PlaceModel createFromParcel(Parcel source) {
            return new PlaceModel(source);
        }

        @Override
        public PlaceModel[] newArray(int size) {
            return new PlaceModel[size];
        }
    };
}

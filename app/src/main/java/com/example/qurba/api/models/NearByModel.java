package com.example.qurba.api.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NearByModel {

    @SerializedName("type")
    private String type;

    @SerializedName("payload")
    private List<PlaceModel> payload;

    public String getType() {
        return type;
    }

    public List<PlaceModel> getPayload() {
        return payload;
    }
}

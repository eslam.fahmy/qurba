package com.example.qurba.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.example.qurba.R;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import androidx.core.content.ContextCompat;

public class GoogleMapMarker {


    public static CircleOptions getCircleOptions(Context context, LatLng latlng) {
        CircleOptions co = new CircleOptions();
        co.center(latlng);
        co.radius(500);
        co.strokeWidth(8);
        co.fillColor(context.getResources().getColor(R.color.colorAccent));
        co.strokeColor(context.getResources().getColor(R.color.colorAccent));
        return co;
    }
/*
    public static MarkerOptions getMarkerBitmapFromView(Context context, LatLng point, int category) {


        if (context == null)
            return new MarkerOptions().position(point);


        View customMarkerView = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.map_marker, null);

        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
        customMarkerView.buildDrawingCache();
        Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(returnedBitmap);
        customMarkerView.draw(canvas);

        MarkerOptions marker = new MarkerOptions();
        marker.position(point);
        marker.icon(BitmapDescriptorFactory.fromBitmap(returnedBitmap));
        return marker;
    }
*/

}

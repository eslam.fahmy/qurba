package com.example.qurba.api.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class CountryModel implements Parcelable {

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private CountryNameModel name;

    public String getId() {
        return id;
    }

    public CountryNameModel getName() {
        return name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeParcelable(this.name, flags);
    }

    public CountryModel() {
    }

    protected CountryModel(Parcel in) {
        this.id = in.readString();
        this.name = in.readParcelable(CountryNameModel.class.getClassLoader());
    }

    public static final Parcelable.Creator<CountryModel> CREATOR = new Parcelable.Creator<CountryModel>() {
        @Override
        public CountryModel createFromParcel(Parcel source) {
            return new CountryModel(source);
        }

        @Override
        public CountryModel[] newArray(int size) {
            return new CountryModel[size];
        }
    };
}

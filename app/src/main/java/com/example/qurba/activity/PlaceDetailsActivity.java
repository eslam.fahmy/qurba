package com.example.qurba.activity;

import android.os.Build;
import android.os.Bundle;
import android.transition.TransitionInflater;
import android.view.View;

import com.example.qurba.R;
import com.example.qurba.adapter.CategoriesAdapter;
import com.example.qurba.adapter.FacilitiesAdapter;
import com.example.qurba.animation.CustomAnimation;
import com.example.qurba.api.models.PlaceModel;
import com.example.qurba.databinding.ActivityDetailsBinding;
import com.example.qurba.utils.Constants;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class PlaceDetailsActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener {


    private ActivityDetailsBinding binding;
    private PlaceModel placeModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_details);
        binding.imgBack.setOnClickListener(this);
        Bundle extras = getIntent().getExtras();
        assert extras != null;
        placeModel = extras.getParcelable(Constants.PAYLOAD);
        binding.setPlace(placeModel);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setSharedElementEnterTransition(TransitionInflater.from(this).inflateTransition(R.transition.shared_element_transaction));
            String imageTransitionName = extras.getString(Constants.TRANSITION);
            binding.img.setTransitionName(imageTransitionName);
            binding.imgBackGround.setTransitionName(imageTransitionName);

        }
        bindData();
    }

    private void bindData() {

        CustomAnimation.scaleOutWithPulse(binding.tvName, 900, 400);
        CustomAnimation.scaleOutWithPulse(binding.tvIsOpen, 1000, 400);
        CustomAnimation.scaleOutWithPulse(binding.rate, 1100, 400);
        CustomAnimation.scaleOutWithPulse(binding.btnFollow, 1200, 400);


        FacilitiesAdapter facilitiesAdapter = new FacilitiesAdapter(this, placeModel.getFacilities());
        binding.rvFacilities.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        binding.rvFacilities.setAdapter(facilitiesAdapter);


        CategoriesAdapter categoriesAdapter = new CategoriesAdapter(this, placeModel.getCategories());
        binding.rvCategories.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        binding.rvCategories.setAdapter(categoriesAdapter);


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);

        if (placeModel.isOpenNow()) {
            binding.tvIsOpen.setText(getResources().getString(R.string.open_now));
            binding.tvIsOpen.setTextColor(getResources().getColor(R.color.green));
        }
        {
            binding.tvIsOpen.setText(getResources().getString(R.string.close));
            binding.tvIsOpen.setTextColor(getResources().getColor(R.color.colorAccent));
        }
        if (placeModel.isFollowedByUser())
            binding.btnFollow.setText(getResources().getString(R.string.following));
        else
            binding.btnFollow.setText(getResources().getString(R.string.follow));


    }


    @Override
    public void onClick(View view) {
        if (view == null)
            return;
        if (view.getId() == R.id.imgBack) {
            onBackPressed();
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        LatLng currentLocation = new LatLng(29.990635011639377, 31.266772149088958);
        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(currentLocation, Float.parseFloat("14.1"));
        googleMap.animateCamera(location);
        MarkerOptions marker = new MarkerOptions();
        marker.position(currentLocation);
        googleMap.addMarker(marker);


    }

}

package com.example.qurba.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.qurba.api.models.AuthenticatePayloadModel;

import androidx.annotation.NonNull;

import static android.content.Context.MODE_PRIVATE;

public class UtilsPreference {


    private SharedPreferences sharedPreferences;

    @NonNull
    public static UtilsPreference getInstance(Context context) {
        return new UtilsPreference(context);
    }

    private UtilsPreference(Context context) {
        sharedPreferences = context.getSharedPreferences(context.getApplicationContext().getPackageName(), MODE_PRIVATE);
    }


    // save Data ..
    private void savePreference(String key, String value) {
        sharedPreferences.edit().putString(key, value).apply();
    }

    public void savePreference(String key, int value) {
        sharedPreferences.edit().putInt(key, value).apply();
    }

    public void savePreference(String key, boolean value) {
        sharedPreferences.edit().putBoolean(key, value).apply();
    }

    // get Data ..
    private String getPreference(String key, String defaultValue) {
        return sharedPreferences.getString(key, defaultValue);
    }

    public int getPreference(String key, int defaultValue) {
        return sharedPreferences.getInt(key, defaultValue);
    }

    public boolean getPreference(String key, boolean defaultValue) {
        return sharedPreferences.getBoolean(key, defaultValue);
    }


    public AuthenticatePayloadModel getUser() {
        AuthenticatePayloadModel user = new AuthenticatePayloadModel();
        user.set_id(getPreference(Constants.USER_ID, null));
        user.setJwt(getPreference(Constants.USER_TOKEN, null));
        user.setRole(getPreference(Constants.USER_ROLE, null));
        return user;
    }

    public void saveUser(AuthenticatePayloadModel user) {
        savePreference(Constants.USER_ID, user.get_id());
        savePreference(Constants.USER_TOKEN, user.getJwt());
        savePreference(Constants.USER_ROLE, user.getRole());
    }
}

package com.example.qurba.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.qurba.R;
import com.example.qurba.animation.CustomAnimation;
import com.example.qurba.api.models.FacilityModel;
import com.example.qurba.databinding.RvFacilitiesItemBinding;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class FacilitiesAdapter extends RecyclerView.Adapter<FacilitiesAdapter.ViewHolder> {

    private Activity context;
    private List<FacilityModel> list;
    private int animationScaleFactor = 800;

    public FacilitiesAdapter(Activity context, List<FacilityModel> list) {
        this.context = context;
        this.list = list;

    }

    @NonNull
    @Override
    public FacilitiesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RvFacilitiesItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.rv_facilities_item, parent, false);
        return new FacilitiesAdapter.ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final FacilitiesAdapter.ViewHolder holder, final int position) {
        if (position != RecyclerView.NO_POSITION)
            holder.setViewModel(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private RvFacilitiesItemBinding binding;

        ViewHolder(final RvFacilitiesItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }

        void bind() {
            if (binding == null) {
                binding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (binding != null) {
                binding.unbind();
            }
        }

        void setViewModel(FacilityModel model) {
            if (binding != null) {
                binding.setFacility(model);
            //    CustomAnimation.scaleOutWithPulse(binding.layoutContainer, animationScaleFactor, 300);
            //    animationScaleFactor = animationScaleFactor + 100;
            }
        }

    }


    @Override
    public void onViewAttachedToWindow(@NonNull FacilitiesAdapter.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull FacilitiesAdapter.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }


}




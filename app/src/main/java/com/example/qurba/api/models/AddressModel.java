package com.example.qurba.api.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class AddressModel implements Parcelable {

    @SerializedName("firstLine")
    private NameLanguage firstLine;

    @SerializedName("country")
    private CountryModel country;

    @SerializedName("city")
    private CountryModel city;

    @SerializedName("area")
    private CountryModel area;

    public NameLanguage getFirstLine() {
        return firstLine;
    }

    public CountryModel getCountry() {
        return country;
    }

    public CountryModel getCity() {
        return city;
    }

    public CountryModel getArea() {
        return area;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.firstLine, flags);
        dest.writeParcelable(this.country, flags);
        dest.writeParcelable(this.city, flags);
        dest.writeParcelable(this.area, flags);
    }

    public AddressModel() {
    }

    protected AddressModel(Parcel in) {
        this.firstLine = in.readParcelable(NameLanguage.class.getClassLoader());
        this.country = in.readParcelable(CountryModel.class.getClassLoader());
        this.city = in.readParcelable(CountryModel.class.getClassLoader());
        this.area = in.readParcelable(CountryModel.class.getClassLoader());
    }

    public static final Parcelable.Creator<AddressModel> CREATOR = new Parcelable.Creator<AddressModel>() {
        @Override
        public AddressModel createFromParcel(Parcel source) {
            return new AddressModel(source);
        }

        @Override
        public AddressModel[] newArray(int size) {
            return new AddressModel[size];
        }
    };
}
